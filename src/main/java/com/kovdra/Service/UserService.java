package com.kovdra.Service;

import com.kovdra.Repositories.UsersRepository;
import com.kovdra.Models.Users;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService {

  @Autowired
  private UsersRepository usersRepository;

  public Collection<Users> getAllUsers(){
    return this.usersRepository.findAll();
  }

  public Users getUserById(ObjectId id){
    //* TODO check that user exist *//*
    //* TODO provide the business logic here*//*
    return this.usersRepository.findBy_id(id);
  }

  public void removeUserByID(ObjectId id) {
    //* TODO check that user exist *//*
    //* TODO provide the business logic here*//*
    this.usersRepository.delete(this.usersRepository.findBy_id(id));
  }

  public void updateUser(ObjectId id, Users user){
    user.set_id(id);
    this.usersRepository.save(user);
  }

  public Users insertUser(Users user) {
    user.set_id(ObjectId.get());
    this.usersRepository.save(user);
    return user;
  }
}
