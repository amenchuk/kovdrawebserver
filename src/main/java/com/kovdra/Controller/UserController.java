package com.kovdra.Controller;

import com.kovdra.Models.Users;
import com.kovdra.Service.UserService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired
  private UserService userService;

  @RequestMapping(method = RequestMethod.GET)
  public Collection<Users> getAllUsers(){
    return userService.getAllUsers();
  }

  @RequestMapping(value = "/{id}",method = RequestMethod.GET)
  public Users getUserById(@PathVariable("id") ObjectId id){
    return userService.getUserById(id);
  }

  @RequestMapping(value = "/delete/{id}",method = RequestMethod.DELETE)
  public void deleteUserById(@PathVariable("id") ObjectId id){
    userService.removeUserByID(id);
  }

  @RequestMapping(value="/update/{id}",method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
  public void updateUser(@PathVariable("id") ObjectId id, @Valid @RequestBody Users user){
    userService.updateUser(id, user);
  }

  @RequestMapping(value="/insert",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public Users insertUser(@Valid @RequestBody Users user){
    return userService.insertUser(user);
  }

}
