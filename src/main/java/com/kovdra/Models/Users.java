package com.kovdra.Models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Users {

  @Id
  private ObjectId _id;
  private String userName;
  private String password;
  private boolean alive;

  public Users() {
  }

  public Users(ObjectId _id, String userName, String password, boolean alive) {
    this._id = _id;
    this.userName = userName;
    this.password = password;
    this.alive = alive;
  }

  public String get_id() {
    return _id.toHexString();
  }

  public void set_id(ObjectId _id) {
    this._id = _id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isAlive() {
    return alive;
  }

  public void setAlive(boolean alive) {
    this.alive = alive;
  }
}
